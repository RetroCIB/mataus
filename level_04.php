<div class="row">
    <div class="col-xs-6 col-sm-9 col-md-9 col-lg-9  row-xs-6 row-sm-3 row-md-3 row-lg-3">
        <div class="row-content-box w3-theme-light">
            <span style="padding:20px 40px; font-size:20px;">
                Tencuiala decorativa este o alegere eficienta daca doriti sa va schimbati aspectul estetic al casei dumneavoastra. Casa visurilor poate fi cu un pas mai aproape, alegand dintr-o gama variata de culori si tipuri de granulatie, in functie de efectul dorit. In culori deschise care dau senzatia de lumina si spatiu sau in culori inchise, contrastante, tencuielile decorative pot fi o optiune atat pentru interior, cat si pentru exterior.
            </span>
        </div>
    </div>
    <div class="col-xs-6 col-sm-3 col-md-3 col-lg-3  row-xs-6 row-sm-3 row-md-3 row-lg-3">
        <div class="row-content-box w3-theme-d4">
            4
        </div>
    </div>
</div>