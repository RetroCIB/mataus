function myPointer() {
    var mapOptions = {
        center:new google.maps.LatLng(
            google_maps["latitude"],
            google_maps["longitude"]
        ),
        zoom:google_maps["zoom"],
        mapTypeId:google.maps.MapTypeId.TERRAIN,
        zoomControl: true,
        panControl: false,
        scaleControl: false,
        mapTypeControl:false,
        streetViewControl:false,
        overviewMapControl:false,
        rotateControl:false
    };

    var map = new google.maps.Map(document.getElementById("map"),mapOptions);

    var marker = new google.maps.Marker({
        position: new google.maps.LatLng(
            google_maps["latitude"],
            google_maps["longitude"]
        ),
        map: map,
    });
}


google.maps.event.addDomListener(window, 'load', myPointer);