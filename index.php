<?php
    $DATA = json_decode(file_get_contents("data/data.json"), true);
    $GALLERY = json_decode(file_get_contents("data/gallery.json"), true);
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Andrei Mataus</title>

    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/bootstrap-theme.min.css" rel="stylesheet">
    <link href="css/slick.css" rel="stylesheet">
    <link href="css/style.css" rel="stylesheet">

</head>
<body>

    <div class="container">
        <?php
        for($i=1; $i<=9; $i++){
            require_once "level_0". $i.".php";
        }
        ?>
    </div>



    <script src="js/jquery.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/slick.min.js"></script>

    <script src="js/goToID.js"></script>

    <script>
        var google_maps = {
            "latitude": <?= $DATA['google_maps']['latitude'] ?>,
            "longitude": <?= $DATA['google_maps']['longitude'] ?>,
            "zoom": <?= $DATA['google_maps']['zoom'] ?>
        }
    </script>
    <script src = "https://maps.googleapis.com/maps/api/js?key=AIzaSyCNbH422Q-7Owj-b2bOB6vdWJXxYWlqmXc&language=ro"></script>
    <script src="js/myPointer.js"></script>
</body>
</html>