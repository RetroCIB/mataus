<?php
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;

require 'lib/php-mailer/src/Exception.php';
require 'lib/php-mailer/src/PHPMailer.php';
require 'lib/php-mailer/src/SMTP.php';