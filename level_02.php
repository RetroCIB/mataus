<div id="level_02" class="row">
    <div class="col-xs-6 col-sm-3 col-md-3 col-lg-3">
        <div class="row">
            <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6   row-xs-6 row-sm-6 row-md-6 row-lg-6  w3-theme-d1">
                <div class="circle-icon w3-theme-l5" onclick="goToID('sectionMail')">
                    <i class="icon icon-Mail"></i>
                </div>
            </div>
            <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6   row-xs-6 row-sm-6 row-md-6 row-lg-6  w3-theme-d2">
                <div class="circle-icon w3-theme-l5" onclick="goToID('sectionPointer')">
                    <i class="icon icon-Pointer"></i>
                </div>
            </div>
        </div>
    </div>
    <div class="col-xs-6 col-sm-9 col-md-9 col-lg-9">
        <div class="row">
            <div class="col-xs-6 col-sm-2 col-md-2 col-lg-2   row-xs-6 row-sm-2 row-md-2 row-lg-2  w3-theme-l1  hidden-xs"></div>
            <div class="col-xs-6 col-sm-2 col-md-2 col-lg-2   row-xs-6 row-sm-2 row-md-2 row-lg-2  w3-theme-l2  hidden-xs"></div>
            <div class="col-xs-6 col-sm-2 col-md-2 col-lg-2   row-xs-6 row-sm-2 row-md-2 row-lg-2  w3-theme-l1  hidden-xs"></div>
            <div class="col-xs-6 col-sm-2 col-md-2 col-lg-2   row-xs-6 row-sm-2 row-md-2 row-lg-2  w3-theme-l2  hidden-xs"></div>
            <div class="col-xs-6 col-sm-2 col-md-2 col-lg-2   row-xs-6 row-sm-2 row-md-2 row-lg-2  w3-theme-l1">
                <div class="circle-icon w3-theme-l5" onclick="goToID('sectionPicture')">
                    <i class="icon icon-Picture"></i>
                </div>
            </div>
            <div class="col-xs-6 col-sm-2 col-md-2 col-lg-2   row-xs-6 row-sm-2 row-md-2 row-lg-2  w3-theme-l2">
                <div class="circle-icon w3-theme-l5" onclick="goToID('sectionWorldWide')">
                    <i class="icon icon-WorldWide"></i>
                </div>
            </div>
        </div>
    </div>
</div>